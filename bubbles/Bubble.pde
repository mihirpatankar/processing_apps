class Bubble{
  //Data for bubbles
  float x;
  float y;
  float diameter;
  int fillColor;
  
  //Constructors
  Bubble(){
    this.x=width/2;
    this.y=height;
    this.diameter=random(50,120);
    this.fillColor=127;
  }
  
  Bubble(float diameter){
    this.x=width/2;
    this.y=height;
    this.diameter=diameter;
    this.fillColor=127;
  }
  
  Bubble(float diameter, int fillColor){
    this.x=width/2;
    this.y=height;
    this.diameter=diameter;
    this.fillColor=fillColor;  
  }
  
  //Functions
  void ascend(){
    this.y+=random(-3,-1);
    this.x+=random(-2,2);
  }
  
  void checkTopBoundary(){
    if(this.y < (this.diameter/2)){
      this.y=this.diameter/2;
    }
  }
  
  void checkSideBoundary(){
    if(this.x < (this.diameter/2)){
      this.x=this.diameter/2;
    }
    else if(this.x > (width-(this.diameter/2))){
      this.x = (width-(this.diameter/2));
    }
  }
  
  void display(){
    stroke(0);
    fill(this.fillColor);
    ellipse(this.x,this.y,this.diameter,this.diameter);
  }
}