ArrayList<Bubble> bubbles;

//Initialization
void setup(){
  fullScreen();
  background(255);
  bubbles = new ArrayList<Bubble>();
}

//Update all bubbles in the list
void draw(){
  background(255);
  for(int i=0;i<bubbles.size();i++){
    bubbles.get(i).ascend();
    bubbles.get(i).display();
    bubbles.get(i).checkTopBoundary();
    bubbles.get(i).checkSideBoundary();
  }
}

//Create bubbles on every tap and add them to list
void mousePressed(){
  Bubble b = new Bubble();
  bubbles.add(b);
}