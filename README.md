# Processing Applications

Following apps are developed using [Processing](https://processing.org/) android mode.

## Developed Apps
* **Hello World** : Displays Hello World on screen
* **Bubbles** : Creates bubbles of random size on each tap which ascend
* **Minesweeper** : 10 x 10 grid with 10 mines
* **Flappy Bird** : Player controls a bird, attempting to fly between columns of green pipes without hitting them
* **Analog Clock** : Minimal RGB analog clock
* **Pong** : Play pong against AI
* **Brick breaker** : Hit the ball with paddle to break all bricks
* **Accel Ball** : Avoid obstacles and reach other end of the screen using tilt

## Under Development
* 2048
* Digit Recognizer

## To do List
* Snake
* Warewolves
* Calculator

## Enhancements and Features to be added
**Tic Tac Toe**
* Wifi/ Bluetooth support for 2 player game

**Minesweeper**
* User selects number of mines and size of board 

## Instructions to Run the App
* Download [processing](https://processing.org/download/)
* Go to Android mode. Using it for the first time requires setup.
* On your android phone go to Settings --> Developer options and enable USB Debugging
* Connect your phone to the laptop/ PC. The phone should be displayed in Android --> Devices tab
* Click on Run button. This should build and upload the app on your phone.

## Contact information
* Mihir Patankar
* mihirpatankar17@gmail.com
