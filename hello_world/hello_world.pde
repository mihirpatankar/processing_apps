PFont f;

void setup(){
  fullScreen();
  noStroke();
  fill(0);
  background(255);
  f = createFont("Arial",16,true);
  textFont(f,16);
}

void draw(){
  text("Hello World!",10,100);
}