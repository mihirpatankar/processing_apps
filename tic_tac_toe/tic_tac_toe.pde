Board b;

void setup(){
  fullScreen();
  int rows=3;
  int cols=3;
  b=new Board(rows,cols);
  b.createBoard();
}

void draw(){
  background(255);
  b.updateDisplay();
  b.checkWin();
}

void mouseReleased(){
  b.updateOnTouch(mouseX,mouseY);
}