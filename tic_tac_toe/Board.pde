class Board{
  int rows;
  int cols;
  int turn;
  Cell[][] board;
  
  Board(int rows,int cols){
    this.rows=rows;
    this.cols=cols;
    this.turn=0;
    board = new Cell[this.rows][this.cols];
  }
  void createBoard(){
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        this.board[i][j]=new Cell(j*(width/this.cols),i*(height/this.rows),width/this.cols,height/this.rows);
      }
    }
  }
  void updateDisplay(){
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        this.board[i][j].display();
      }
    }
  }
  
  void updateTurn(){
    this.turn=1-this.turn;
  }
  
  void updateOnTouch(float x,float y){
    updateTurn();
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        if(this.board[i][j].containsPoint(x,y)){
          this.board[i][j].updateData(this.turn);
        }
      }
    }
  }
  
  void checkWin(){
    for(int i=0;i<this.rows;i++){
      if(!(!board[i][0].filled || !board[i][1].filled || !board[i][2].filled)){ 
        if(board[i][0].data == board[i][1].data && board[i][0].data == board[i][2].data){
          changeFillColor(this.board[i][0]);
          changeFillColor(this.board[i][1]);
          changeFillColor(this.board[i][2]);
        }
      }
    }
    for(int j=0;j<this.cols;j++){
      if(!(!board[0][j].filled || !board[1][j].filled || !board[2][j].filled)){
        if(board[0][j].data == board[1][j].data && board[0][j].data == board[2][j].data){
          changeFillColor(this.board[0][j]);
          changeFillColor(this.board[1][j]);
          changeFillColor(this.board[2][j]);
        }
      }
    }
    if(!(!board[0][0].filled || !board[1][1].filled || !board[2][2].filled)){
    
      if(board[0][0].data == board[1][1].data && board[0][0].data == board[2][2].data){
        changeFillColor(this.board[0][0]);
        changeFillColor(this.board[1][1]);
        changeFillColor(this.board[2][2]);
      }
    }
    
    if(!(!board[0][2].filled || !board[1][1].filled || !board[2][0].filled)){
      if(board[0][2].data == board[1][1].data && board[0][2].data == board[2][0].data){
        changeFillColor(this.board[0][2]);
        changeFillColor(this.board[1][1]);
        changeFillColor(this.board[2][0]);
      }
    }
  }
  
  void changeFillColor(Cell c){
    c.fillColor[0]=0;
    c.fillColor[2]=0;
  }
}