class Cell{
  float x;
  float y;
  float w;
  float h;
  char data;
  boolean filled;
  int[] fillColor;
  
  Cell(float x,float y,float w,float h){
    this.x=x;
    this.y=y;
    this.w=w;
    this.h=h;
    this.data='b';
    this.filled=false;
    this.fillColor=new int[3];
    for(int i=0;i<3;i++){
      this.fillColor[i]=255;
    }
  }
  
  void display(){
    stroke(0);
    noFill();
    rect(this.x,this.y,this.w,this.h);
    
    if(this.filled){
      fill(this.fillColor[0],this.fillColor[1],this.fillColor[2]);
      rect(this.x,this.y,this.w,this.h);
      fill(0);
      float min = this.w > this.h ? this.h : this.w;
      textAlign(CENTER,CENTER);
      textSize(min*0.7);
      text(this.data,this.x+this.w/2,this.y+this.h/2);
    }
  }
  
  
  void updateData(int turn){
    if(!this.filled){
      this.filled=true;
      if(turn==0){
        this.data='X';
      }
      else{
        this.data='O';
      }
    }
  }
  
  boolean containsPoint(float x,float y){
    return (x > this.x && x< this.x+this.w && y > this.y && y < this.y+this.h);
  }
   
  
  
  
  
  
  
}