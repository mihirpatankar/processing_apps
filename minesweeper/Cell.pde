class Cell{
	//Data for cells
	float x;
	float y;
	float w;
  float h;
  int[] mineFillColor;
  int mineCount;
	boolean mine;
	boolean revealed;
  boolean marked;

	//Constructors
	Cell(float x,float y,float w,float h){
    this.x=x;
    this.y=y;
    this.w=w;
    this.h=h;
    this.mineFillColor = new int[3];
    for(int i=0;i<3;i++){
      this.mineFillColor[i]=255;
    }
    this.mineCount=0;
    this.mine=false;
    this.revealed=false;
    this.marked=false;
	}

  void display(){
    stroke(0);
    noFill();
    rect(this.x,this.y,this.w,this.h);
    
    if(this.revealed){
      if(this.mine || this.marked){
        fill(this.mineFillColor[0],this.mineFillColor[1],this.mineFillColor[2]);
        rect(this.x,this.y,this.w,this.h);
        stroke(0);
        fill(0);
        ellipse(this.x+this.w/2,this.y+this.h/2,this.w/2,this.h/2);
      }
      else{
        fill(200);
        rect(this.x,this.y,this.w,this.h);
        fill(0);
        if(this.mineCount!=0){
          float min = this.w > this.h ? this.h : this.w;
          textSize(min*0.7);
          textAlign(CENTER,CENTER);
          text(this.mineCount,this.x+this.w/2,this.y+this.h/2);
        }
      }
    }
  }
  
  boolean containsPoint(float x,float y){
    return (x > this.x && x< this.x+this.w && y > this.y && y < this.y+this.h);
  }
  
  void reveal(){
    if(!this.revealed){
      this.revealed=true;
    }
  }
  
  void toggleMarkMine(){
    if(!(this.marked ^ this.revealed)){
      this.marked = !this.marked;
      this.revealed = !this.revealed;
    }
  }
  
  void markMineRed(){
    this.mineFillColor[0]=255;
    this.mineFillColor[1]=0;
    this.mineFillColor[2]=0;
  }
  
  void markMineGreen(){
    this.mineFillColor[0]=0;
    this.mineFillColor[1]=255;
    this.mineFillColor[2]=0;
  }
  
  void markMineYellow(){
    this.mineFillColor[0]=255;
    this.mineFillColor[1]=255;
    this.mineFillColor[2]=0;
  }
 
}