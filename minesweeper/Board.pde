class Board{
  //Data structure
  int rows;
  int cols;
  int mines;
  int cellsToBeRevealed;
  boolean gameOver;
  boolean playerWin;
  Cell[][] board;
  
  Board(int rows,int cols,int mines){
    this.rows=rows;
    this.cols=cols;
    this.mines=mines;
    this.cellsToBeRevealed=this.rows * this.cols;
    this.gameOver=false;
    this.playerWin=false;
    this.board = new Cell[this.rows][this.cols];
  }  
  
  void createBoard(){
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        this.board[i][j]=new Cell(j*(width/this.cols),i*(height/this.rows),width/this.cols,height/this.rows);
      }
    }
  }
  
  void placeMines(){
    int mineCount=0;
    while(mineCount!=this.mines){
      int i=(int)random(0,this.rows);
      int j=(int)random(0,this.cols);
      if(!this.board[i][j].mine){
        this.board[i][j].mine=true;
        mineCount++;
      } 
    }
  }
  
  void updateDisplay(){
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        this.board[i][j].display();
      }
    }
  }

  
  void revealOnTouch(float x,float y){
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        if(this.board[i][j].containsPoint(x,y) && !this.board[i][j].marked){
          if(this.board[i][j].mine){
            this.gameOver();
          }
          else if(this.board[i][j].mineCount==0){
            this.floodFill(i,j);
          }
          else{
            this.cellsToBeRevealed--;
            this.board[i][j].reveal();
          }
        }
      }
    }
  }
  
  void onLongTouch(float x,float y){
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        if(this.board[i][j].containsPoint(x,y)){
          this.board[i][j].toggleMarkMine();
          this.board[i][j].markMineYellow();
        }
      }
    }
  }
  
  void updateMineCount(){
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        this.board[i][j].mineCount = countMines(i,j);
      }
    }
  }
  
  int countMines(int row,int col){
    int mineCount=0;
    for(int i=-1;i<=1;i++){
      for(int j=-1;j<=1;j++){
        if(i==0 && j==0){
          continue;
        }
        if(row+i > -1 && row+i<this.rows && col+j > -1 && col+j < this.cols){
          if(board[row+i][col+j].mine){
            mineCount++;
          }
        }
      }
    }
    return mineCount;
  }
 
  void floodFill(int row,int col){
    if(!this.board[row][col].revealed){
      this.board[row][col].reveal();
      this.cellsToBeRevealed--;
      if(this.board[row][col].mineCount==0){
        for(int i=-1;i<=1;i++){
          for(int j=-1;j<=1;j++){
            if(i==0 && j==0){
              continue;
            }
            if(row+i > -1 && row+i<this.rows && col+j > -1 && col+j < this.cols){
              floodFill(row+i,col+j);
            }
          }
        }
      }
    }
  }
  
  void gameOver(){
    for(int i=0;i<this.rows;i++){
      for(int j=0;j<this.cols;j++){
        this.board[i][j].reveal();
        if(this.board[i][j].mine){
          this.board[i][j].markMineRed();
        }
      }
    }
    this.gameOver=true;
    float min = width > height ? height : width;
    textSize(min*0.15);
    textAlign(CENTER,CENTER);
    text("GAME OVER",width/2,height/2);
  }
  
  void checkWin(){
    if(this.cellsToBeRevealed == this.mines){
      for(int i=0;i<this.rows;i++){
        for(int j=0;j<this.cols;j++){
          this.board[i][j].reveal();
          if(this.board[i][j].mine){
            this.board[i][j].markMineGreen();
          }
        }
      }
      this.playerWin=true;
      float min = width > height ? height : width;
      textSize(min*0.15);
      textAlign(CENTER,CENTER);
      text("YOU WIN",width/2,height/2);
    }
  }
  
  void reset(){
    b = new Board(rows,cols,mines);
    b.createBoard();
    b.placeMines();
    b.updateMineCount();
  }

}