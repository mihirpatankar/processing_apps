Board b;
int start,timer;
int rows=10;
int cols=10;
int mines=10;
void setup(){
  fullScreen();
  //TODO
  //Make the user select number of rows, cols and mines
  b=new Board(rows,cols,mines);
  b.createBoard();
  b.placeMines();
  b.updateMineCount();
}

void draw(){
  background(255);
  b.updateDisplay();
  b.checkWin();
  if(b.gameOver){
    b.gameOver();
  }
}

void mousePressed(){
  start=millis();
}
void mouseReleased(){
  int timer = millis()-start;
  if(!(b.gameOver || b.playerWin)){
    if(timer>=500){
      b.onLongTouch(mouseX,mouseY);
    }
    else{
      b.revealOnTouch(mouseX,mouseY);
    }
  }
  else{
    b.reset();
  }
}